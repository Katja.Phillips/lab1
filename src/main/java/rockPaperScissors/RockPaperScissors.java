package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    // ---------- ---------- ---------- ---------- ----------
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    // ---------- ---------- ---------- ---------- ----------

    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);

            // Human and computer choices
            String human_choice = user_choice();
            String computer_choice = random_choice(rpsChoices);
            String choice_string = "Human chose " + human_choice + ", computer chose " + computer_choice + ".";

            // Check who won and updating score
            if (is_winner(human_choice, computer_choice)) {
                System.out.println(choice_string + " Human wins.");
                humanScore += 1;
            }
            else if (is_winner(computer_choice, human_choice)) {
                System.out.println(choice_string + " Computer wins.");
                computerScore += 1;
            }
            else {
                System.out.println(choice_string + " It's a tie!");
            }

            // Score tracking
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Play again?
            String continue_answer = continue_playing(); 
            // if user answers n (or N), then game stops
            if (continue_answer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

            roundCounter += 1;
        }
    }

    public String user_choice() {
        while (true) {
            String human_choice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            // System.out.println(human_choice);
            // boolean valid_human_choice = validate_input(human_choice, rpsChoices);
            // System.out.println(valid_human_choice);
            if (validate_input(human_choice, rpsChoices)) {
                return human_choice;
            }
            else {System.out.println("I don't understand " + human_choice + ". Try again");
            }
        }
    }

    public String random_choice(List<String> choices_list) {
        // System.out.println(choices_list);

        /* https://www.codegrepper.com/code-examples/java/
            get+random+String+from+array+list 
        Choosing random index and 
        extracting the element from the list at the chosen index */
        Random r = new Random();
        int random_index = r.nextInt(choices_list.size());
        String choice = choices_list.get(random_index);

        // System.out.println(choice);
        return choice;
    }

    public boolean is_winner(String choice1, String choice2) {
        // Returns true if choice1 wins over choice2
        if (choice1 == "paper") {
            return choice2 == "rock";
        }
        else if (choice1 == "scissors") {
            return choice2 == "paper";
        }
        else {
            return choice2 == "scissors";
        }
    }

    // Ask if human want's to continue playing and return "y" or "n"
    public String continue_playing() {
        while (true) {
            String continue_answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validate_input(continue_answer, Arrays.asList("y", "n"))) {
                return continue_answer;
            }
            else {
                System.out.println("I don't understand " + continue_answer + ". Try again");
            }
        }
    }
    
    public static boolean validate_input(String input, List<String> valid_input) {
        // input = input.toLowerCase();
        // Check if input is found in the list of valid_input
        return valid_input.contains(input);
    }

    // ---------- ---------- ---------- ---------- ----------
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    // ---------- ---------- ---------- ---------- ----------
}  